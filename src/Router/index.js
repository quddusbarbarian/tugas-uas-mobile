import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Splash,
  Home,
  Doa,
  DoaMakan,
  DoaTidur,
  DoaBelajar,
  Surah,
} from '../Page';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      {/* bagiandoa */}
      <Stack.Screen name="Doa" component={Doa} />
      <Stack.Screen name="DoaMakan" component={DoaMakan} />
      <Stack.Screen name="DoaTidur" component={DoaTidur} />
      <Stack.Screen name="DoaBelajar" component={DoaBelajar} />
      {/* bagian surah-surah */}
      <Stack.Screen name="Surah" component={Surah} />
    </Stack.Navigator>
  );
};

export default Router;
