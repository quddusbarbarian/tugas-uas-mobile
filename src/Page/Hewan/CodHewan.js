import React from 'react';
import {View, Text, Image} from 'react-native';

const CodHewan = props => {
  return (
    <View
      style={{
        borderColor: '#016937',
        borderWidth: 1,
        borderRadius: 20,
        marginTop: 20,
        height: 90,
        width: 90,
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 10,
        marginLeft: 10,
      }}>
      <Image source={props.img} style={{height: 45, width: 42}} />
      <Text
        style={{
          color: '#016937',
          fontWeight: 'bold',
          textAlign: 'center',
          fontSize: 12,
        }}>
        {props.name}
      </Text>
    </View>
  );
};

export default CodHewan;
