import React from 'react';
import {View, Image, Text, ScrollView} from 'react-native';
import CodHewan from './CodHewan';

const Hewan = () => {
  return (
    <ScrollView>
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          paddingBottom: 15,
        }}>
        <CodHewan img={require('../../Img/Hewan/jerapah.png')} name="jerapah" />
        <CodHewan img={require('../../Img/Hewan/kuda.png')} name="Kuda" />
        <CodHewan img={require('../../Img/Hewan/zebra.png')} name="Zebra" />
        <CodHewan img={require('../../Img/Hewan/kelinci.png')} name="Kelinci" />
        <CodHewan img={require('../../Img/Hewan/singa.png')} name="Singa" />
        <CodHewan img={require('../../Img/Hewan/burung.png')} name="Burung" />
        <CodHewan img={require('../../Img/Hewan/harimau.png')} name="Harimau" />
        <CodHewan img={require('../../Img/Hewan/beruang.png')} name="Beruang" />
        <CodHewan img={require('../../Img/Hewan/sapi.png')} name="Sapi" />
        <CodHewan
          img={require('../../Img/Hewan/serigala.png')}
          name="Serigala"
        />
        <CodHewan img={require('../../Img/Hewan/angsa.png')} name="Angsa" />
        <CodHewan img={require('../../Img/Hewan/ayam.png')} name="Ayam" />
        <CodHewan img={require('../../Img/Hewan/onta.png')} name="Unta" />
        <CodHewan img={require('../../Img/Hewan/kucing.png')} name="Kucing" />
        <CodHewan img={require('../../Img/Hewan/jerapah.png')} name="Hewan" />
        <CodHewan img={require('../../Img/Hewan/jerapah.png')} name="Hewan" />
        <CodHewan img={require('../../Img/Hewan/jerapah.png')} name="Hewan" />
        <CodHewan img={require('../../Img/Hewan/jerapah.png')} name="Hewan" />
      </View>
    </ScrollView>
  );
};

export default Hewan;
