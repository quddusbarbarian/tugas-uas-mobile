import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import CodWarna from './CodWarna';

const Warna = () => {
  return (
    <ScrollView>
      <View
        style={{
          backgroundColor: '#B5B5B5',
          flex: 1,
          paddingHorizontal: 30,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          paddingVertical: 10,
        }}>
        <CodWarna
          name="Ungu"
          backgroun={{backgroundColor: '#9400d3', height: 100, width: 81}}
          color={{
            color: '#9400d3',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Nila"
          backgroun={{backgroundColor: '#4B0082', height: 100, width: 81}}
          color={{
            color: '#4B0082',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Biru"
          backgroun={{backgroundColor: '#0000FF', height: 100, width: 81}}
          color={{
            color: '#0000FF',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Hijau"
          backgroun={{backgroundColor: '#00FF00', height: 100, width: 81}}
          color={{
            color: '#00FF00',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Kuning"
          backgroun={{backgroundColor: '#FFFF00', height: 100, width: 81}}
          color={{
            color: '#FFFF00',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Jingga"
          backgroun={{backgroundColor: '#FF7F00', height: 100, width: 81}}
          color={{
            color: '#FF7F00',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Red"
          backgroun={{backgroundColor: '#FF0000', height: 100, width: 81}}
          color={{
            color: '#FF0000',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Putih"
          backgroun={{backgroundColor: 'white', height: 100, width: 81}}
          color={{
            color: 'white',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Mirah Tua"
          backgroun={{backgroundColor: 'maroon', height: 100, width: 81}}
          color={{
            color: 'maroon',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Abu Abu"
          backgroun={{backgroundColor: 'gray', height: 100, width: 81}}
          color={{
            color: 'gray',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Abu Abu"
          backgroun={{backgroundColor: 'black', height: 100, width: 81}}
          color={{
            color: 'black',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
        <CodWarna
          name="Aqua"
          backgroun={{backgroundColor: 'aqua', height: 100, width: 81}}
          color={{
            color: 'aqua',
            fontWeight: 'bold',
            textTransform: 'capitalize',
          }}
        />
      </View>
    </ScrollView>
  );
};

export default Warna;
