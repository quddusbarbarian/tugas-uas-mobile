import React from 'react';
import {View, Text} from 'react-native';

const CodWarna = props => {
  return (
    <View
      style={{
        alignItems: 'center',
        marginHorizontal: 9,
        marginTop: 10,
      }}>
      <View style={props.backgroun} />
      <Text style={props.color}>{props.name}</Text>
    </View>
  );
};

export default CodWarna;
