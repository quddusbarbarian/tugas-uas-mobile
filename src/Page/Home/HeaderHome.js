import React from 'react';
import {View, Text, Image} from 'react-native';

const HeaderHome = () => {
  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <View
        style={{
          height: 94,
          width: 267,
          marginTop: 20,
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: 10,
          borderWidth: 2,
          borderColor: '#016937',
        }}>
        <Image
          source={require('../../Img/basmalah.png')}
          style={{width: '100%', height: '100%'}}
        />
      </View>
      <View
        style={{
          height: 59,
          width: 212,
          marginTop: 15,
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: 10,
          borderWidth: 2,
          borderColor: '#016937',
        }}>
        <Text style={{color: '#016937', fontWeight: 'bold'}}>
          ANAK PINTTAR_(ANPI)
        </Text>
      </View>
    </View>
  );
};

export default HeaderHome;
