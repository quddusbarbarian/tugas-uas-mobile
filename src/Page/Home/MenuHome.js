import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';

const MenuHome = props => {
  return (
    <TouchableOpacity
      style={{
        height: 73,
        width: 75,
        borderWidth: 2,
        borderColor: '#016937',
        borderRadius: 23,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
      }}
      onPress={props.onPress}>
      <Image source={props.img} style={{height: '49%', width: '49%'}} />
      <Text style={{color: '#016937', fontWeight: '700'}}>{props.name}</Text>
    </TouchableOpacity>
  );
};

export default MenuHome;
