import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import HeaderHome from './HeaderHome';
import MenuHome from './MenuHome';

const Home = ({navigation}) => {
  return (
    <View style={{flex: 1, paddingHorizontal: 15, paddingVertical: 15}}>
      <HeaderHome />
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
        }}>
        <MenuHome
          name="Doa Doa"
          img={require('../../Img/doa.png')}
          onPress={() => navigation.navigate('Doa')}
        />
        <MenuHome
          name="Surah"
          img={require('../../Img/surah.png')}
          onPress={() => navigation.navigate('Surah')}
        />
        <MenuHome name="Warna" img={require('../../Img/warna.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
        <MenuHome name="Warna" img={require('../../Img/doa.png')} />
      </View>
    </View>
  );
};

export default Home;
