import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';

const MenuSurah = props => {
  return (
    <TouchableOpacity
      style={{
        height: 51,
        width: 360,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignContent: 'center',
        paddingTop: 10,
        paddingHorizontal: 10,
        marginBottom: 3,
      }}
      onPress={props.onPress}>
      <Image source={props.img} style={{height: '63%', width: '8%'}} />
      <Text
        style={{
          color: '#016937',
          marginLeft: 13,
          fontSize: 16,
          fontWeight: 'bold',
        }}>
        {props.name}
      </Text>
    </TouchableOpacity>
  );
};

export default MenuSurah;
