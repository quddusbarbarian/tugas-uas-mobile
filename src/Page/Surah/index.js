import React from 'react';
import {View, Text} from 'react-native';
import MenuSurah from './MenuSurah';

const Surah = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#016937', paddingTop: 10}}>
      <MenuSurah img={require('../../Img/surah.png')} name="Surah allahab" />
    </View>
  );
};

export default Surah;
