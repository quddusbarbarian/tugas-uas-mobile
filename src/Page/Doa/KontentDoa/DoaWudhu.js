import React from 'react';
import {View, Text} from 'react-native';

const DoaWudhu = () => {
  return (
    <View style={{flex: 1, padding: 10}}>
      <View>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 25,
            fontWeight: 'bold',
            margin: 5,
          }}>
          Doa Sebelum Wudhu
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          نَوَيْتُ الْوُضُوْءَ لِرَفْعِ الْحَدَثِ اْلاَصْغَرِ فَرْضًا لِلّٰهِ
          تَعَالَى
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          Artinya: "Saya niat berwudhu untuk menghilangkan hadast kecil fardu
          (wajib) karena Allah ta'ala"
        </Text>
      </View>
      <View>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 25,
            fontWeight: 'bold',
            margin: 5,
          }}>
          Doa Sesudah Wudhu
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          اَشْهَدُ اَنْ لاَّاِلَهَ اِلاَّاللهُ وَحْدَهُ لاَشَرِيْكَ لَهُ
          وَاَشْهَدُ اَنَّ مُحَمَّدًاعَبْدُهُ وَرَسُوْلُهُ. اَللّٰهُمَّ
          اجْعَلْنِىْ مِنَ التَّوَّابِيْنَ وَاجْعَلْنِىْ مِنَ
          الْمُتَطَهِّرِيْنَ، وَجْعَلْنِيْ مِنْ عِبَادِكَ الصَّالِحِيْنَ
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          Artinya: "Aku bersaksi, tidak ada Tuhan selain Allah Yang Maha Esa,
          tidak ada sekutu bagi-Nya, dan aku mengaku bahwa Nabi Muhammad itu
          adalah hamba dan Utusan Allah. Ya Allah, jadikanlah aku dari golongan
          orang-orang yang bertaubat dan jadikanlah aku dari golongan
          orang-orang yang suci dan jadikanlah aku dari golongan hamba-hamba Mu
          yang shaleh"
        </Text>
      </View>
    </View>
  );
};

export default DoaWudhu;
