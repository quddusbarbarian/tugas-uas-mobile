import React from 'react';
import {View, Text} from 'react-native';

const DoaSampai = () => {
  return (
    <View style={{flex: 1, padding: 10}}>
      <View>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 25,
            fontWeight: 'bold',
            margin: 5,
          }}>
          Doa Sampai Tempat Tujuan
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          اَلْحَمْدُ ِللهِ الَّذِى سَلَمَنِى وَالَّذِى اَوَنِى وَالَّذِى جَمَعَ
          الشَّمْلَ بِ
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          Artinya: "Segala puji bagi Allah, yang telah menyelamatkan akau dan
          yang telah melindungiku dan yang mengumpulkanku dengan keluargaku."
        </Text>
      </View>
    </View>
  );
};

export default DoaSampai;
