import React from 'react';
import {View, Text} from 'react-native';

const DoaSebelumMandi = () => {
  return (
    <View style={{flex: 1, padding: 10}}>
      <View>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 25,
            fontWeight: 'bold',
            margin: 5,
          }}>
          Doa Sebelum Mandi
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          اَللّٰهُمَّ اغْفِرْلِى ذَنْبِى وَوَسِّعْ لِى فِىْ دَارِىْ وَبَارِكْ
          لِىْ فِىْ رِزْقِىْ
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          Artinya: "Ya Allah ampunilah dosa kesalahanku dan berilah keluasaan di
          rumahku serta berkahilah pada rezekiku."
        </Text>
      </View>
    </View>
  );
};

export default DoaSebelumMandi;
