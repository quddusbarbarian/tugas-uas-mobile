import React from 'react';
import {View, Text} from 'react-native';

const DoaBepergian = () => {
  return (
    <View style={{flex: 1, padding: 10}}>
      <View>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 25,
            fontWeight: 'bold',
            margin: 5,
          }}>
          Doa Sebelum Bepergian
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          اَللّٰهُمَّ هَوِّنْ عَلَيْنَا سَفَرَنَا هَذَا وَاطْوِعَنَّابُعْدَهُ
          اَللّٰهُمَّ اَنْتَ الصَّاحِبُ فِى السَّفَرِوَالْخَلِيْفَةُفِى
          الْاَهْلِ
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: 'green',
            fontSize: 15,
            margin: 5,
          }}>
          Artinya: "Ya Allah, mudahkanlah kami berpergian ini, dan dekatkanlah
          kejauhannya. Ya Allah yang menemani dalam berpergian, dan Engkau pula
          yang melindungi keluarga."
        </Text>
      </View>
    </View>
  );
};

export default DoaBepergian;
