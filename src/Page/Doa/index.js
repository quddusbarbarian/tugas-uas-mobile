import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import MenuDoa from './MenuDoa';

const Doa = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#016937', paddingTop: 10}}>
      <MenuDoa
        img={require('../../Img/doa.png')}
        name="Doa Sebelum Dan Sesudah Makan"
        onPress={() => navigation.navigate('DoaMakan')}
      />
      <MenuDoa
        img={require('../../Img/doa.png')}
        name="Doa Sebelum Dan Sesudah Tidur"
        onPress={() => navigation.navigate('DoaTidur')}
      />
      <MenuDoa
        img={require('../../Img/doa.png')}
        name="Doa Sebelum Dan Sesudah Belajar"
        onPress={() => navigation.navigate('DoaBelajar')}
      />
      <MenuDoa
        img={require('../../Img/doa.png')}
        name="Doa Sebelum Dan Sesudah Belajar"
      />
      <MenuDoa img={require('../../Img/doa.png')} name="Doa Manjur wkwkwk" />
      <MenuDoa img={require('../../Img/doa.png')} name="Doa Manjur wkwkwk" />
      <MenuDoa img={require('../../Img/doa.png')} name="Doa Manjur wkwkwk" />
    </View>
  );
};

export default Doa;
