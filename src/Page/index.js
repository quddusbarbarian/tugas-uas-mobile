import Splash from './Splash';
import Home from './Home';
import Doa from './Doa';
import DoaMakan from './Doa/KontentDoa/DoaMakan';
import DoaTidur from './Doa/KontentDoa/DoaTidur';
import DoaBelajar from './Doa/KontentDoa/DoaBelajar';
import Surah from './Surah';

export {Splash, Home, Doa, DoaMakan, DoaTidur, DoaBelajar, Surah};
