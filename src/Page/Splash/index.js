import React, {useEffect} from 'react';
import {View, Text, Image} from 'react-native';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 2000);
  });
  return (
    <View style={{justifyContent: 'center', backgroundColor: 'white', flex: 1}}>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={require('../../Img/splash.png')}
          style={{height: '58%', width: '59%', alignItems: 'center'}}
        />
      </View>
    </View>
  );
};

export default Splash;
