import React from 'react';
import {View, Text, Image} from 'react-native';

const CodBuah = props => {
  return (
    <View
      style={{
        borderColor: '#016937',
        borderWidth: 1,
        borderRadius: 20,
        marginTop: 20,
        height: 90,
        width: 90,
        alignItems: 'center',
        paddingVertical: 7,
        paddingHorizontal: 10,
        marginLeft: 10,
      }}>
      <Image source={props.img} style={{height: '65%', width: '65%'}} />
      <Text
        style={{
          color: '#016937',
          fontWeight: 'bold',
          textAlign: 'center',
          fontSize: 12,
        }}>
        {props.name}
      </Text>
    </View>
  );
};

export default CodBuah;
