import React from 'react';
import {View, Image, Text, ScrollView} from 'react-native';
import CodBuah from './CodBuah';

const Buah = () => {
  return (
    <ScrollView>
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center',
          paddingBottom: 15,
        }}>
        <CodBuah img={require('../../Img/buah/jeruk.png')} name="Jeruk" />
        <CodBuah img={require('../../Img/buah/melon.png')} name="Melon" />
        <CodBuah img={require('../../Img/buah/nanas.png')} name="Nanas" />
        <CodBuah img={require('../../Img/buah/nangka.png')} name="Nangka" />
        <CodBuah img={require('../../Img/buah/pepaya.png')} name="Pepaya" />
        <CodBuah img={require('../../Img/buah/pisang.png')} name="Pisang" />
        <CodBuah img={require('../../Img/buah/semangka.png')} name="Semangka" />
        <CodBuah img={require('../../Img/buah/stroberi.png')} name="Stroberi" />
        <CodBuah img={require('../../Img/buah/anggur.png')} name="Anggur" />
        <CodBuah img={require('../../Img/buah/apel.png')} name="Apel" />
        <CodBuah img={require('../../Img/buah/delima.png')} name="Delima" />
        <CodBuah img={require('../../Img/buah/jambu.png')} name="Jambu" />
        <CodBuah img={require('../../Img/buah/naga.png')} name="Naga" />
        <CodBuah img={require('../../Img/buah/bluberi.png')} name="Bluberi" />
        <CodBuah img={require('../../Img/buah/mangga.png')} name="Mangga" />
        <CodBuah img={require('../../Img/buah/lemon.png')} name="Lemon" />
        <CodBuah img={require('../../Img/buah/durian.png')} name="Durian" />
        <CodBuah img={require('../../Img/buah/sirsak.png')} name="Sirsak" />
      </View>
    </ScrollView>
  );
};

export default Buah;
