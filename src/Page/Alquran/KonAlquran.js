import React from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';
// import dataJason from '../../../data.json';

const KonAlquran = datas => {
  const konten = datas.route.params.verse;
  console.log(konten);
  return (
    <ScrollView>
      <View>
        {konten.map(item => {
          return (
            <View style={styles.wadah} key={item.number}>
              <Text style={styles.latin}>{item.translation_id}</Text>
              <Text style={styles.text}>
                {item.text}
                <View style={styles.decornumber}>
                  <Text style={styles.decorText}>{item.number}</Text>
                </View>
              </Text>
            </View>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default KonAlquran;

const styles = StyleSheet.create({
  wadah: {
    backgroundColor: 'green',
    padding: 15,
  },
  text: {
    color: 'white',
    fontSize: 20,
    marginTop: 15,
  },
  latin: {
    color: 'white',
    marginBottom: 5,
    fontSize: 15,
  },
  decornumber: {
    padding: 2,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 20,
    marginRight: 10,
    alignItems: 'center',
    alignContent: 'center',
    flex: 1,
  },
  decorText: {
    fontSize: 10,
    color: 'white',
    margin: 2,
    textAlign: 'center',
  },
});
