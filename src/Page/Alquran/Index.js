import React from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import dataJson from '../../../data.json';

const Alquran = ({navigation}) => {
  console.log(dataJson);
  const data = dataJson;
  return (
    <ScrollView>
      <View>
        {data.map(item => {
          return (
            <TouchableOpacity
              style={StyleS.wadah}
              key={item.number_of_surah}
              onPress={() =>
                navigation.navigate('KonAlquran', {
                  verse: item.verses,
                })
              }>
              <Text style={StyleS.Text1}>{item.name}</Text>
              <Text style={StyleS.Text2}>{item.name_translations.ar}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </ScrollView>
  );
};

export default Alquran;
const StyleS = StyleSheet.create({
  wadah: {
    padding: 5,
    marginBottom: 2,
    backgroundColor: 'green',
  },
  Text1: {
    color: 'white',
    fontSize: 15,
    marginLeft: 5,
    fontWeight: 'bold',
  },
  Text2: {
    color: 'white',
    fontSize: 15,
    marginEnd: 5,
    fontWeight: 'bold',
  },
});
